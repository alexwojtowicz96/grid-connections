import java.io.File;
import java.io.FileNotFoundException;

public class Driver {

//=================================================================================================
	public static void main(String[] args) {
		Grid grid = new Grid();
		String fileName = "/Users/alexanderwojtowicz/Desktop/input-grid_3.txt";
		try {
			grid = new Grid(new File(fileName));
		} 
		catch (FileNotFoundException e) {
			System.out.println("ERROR:  Could not find file '" + 
								fileName +
								"'.\nTry using absolute path...\n");
			e.printStackTrace();
		}
		//grid = new Grid();
		grid.display();
		System.out.println(grid.horizontalConnections(2));
		System.out.println(grid.verticalConnections(2));
		System.out.println(grid.findConnections(2));
		System.out.println(grid.maxConnections(9));
		System.out.println(grid.largestConnection());

	}

//=================================================================================================
	
}
