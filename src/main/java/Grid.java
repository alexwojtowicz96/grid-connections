/**
 * A Grid is essentially a matrix of integers which can be used to represent
 * colors. The matrix does not have to be square, and the default Grid will
 * be a 3x3 matrix where each row is a different integer.
 * @author alexanderwojtowicz
 */
import java.util.Vector;
import java.util.Map;
import java.util.Scanner;
import java.util.HashMap;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Set;
import java.util.HashSet;

public class Grid {
	
	private Vector<Vector<Integer>> grid;
	
//=============================================================================	
	/** 
	 * Create the default Grid
	 */
	public Grid() {
		createDefaultGrid();
	}
	
//=============================================================================	
	/**
	 * The default Grid:  1 1 1
	 *                    2 2 2
	 *                    3 3 3
	 */
	public void createDefaultGrid() {
		grid = new Vector<Vector<Integer>>();
		for(int i=0; i<3; i++) {
			Vector<Integer> row = new Vector<Integer>();
			for(int j=0; j<3; j++) {
				row.add(i+1);
			}
			grid.add(row);
		}
	}
	
//=============================================================================
	/**
	 * Given a file with a grid (valid), read it into the Grid::grid
	 * @param gridFile
	 * @throws FileNotFoundException 
	 */
	public Grid(File gridFile) throws FileNotFoundException {
		if(fileIsValid(gridFile)) {
			System.out.println("SUCCESS:  File '" +
								gridFile.toString() + 
								"' has been read successfully!");
			
			grid = new Vector<Vector<Integer>>();
			Scanner reader = new Scanner(gridFile);
			int numRows = 0;
			while(reader.hasNextLine()) {
				numRows += 1;
				//System.out.println(reader.nextLine());
				reader.nextLine();
			}
			reader.close();
			reader = new Scanner(gridFile);
			Vector<Integer> allElems = new Vector<Integer>();
			while(reader.hasNext()) {
				allElems.add(Integer.parseInt(reader.next()));
			}
			reader.close();
			int rowItr = 0;
			final int numInRow = allElems.size() / numRows;
			//System.out.println(numInRow);
			Vector<Integer> row = new Vector<Integer>();
			for(int i=0; i<allElems.size(); i++) {
				row.add(allElems.get(i));
				rowItr += 1;
				if(rowItr == numInRow) {
					grid.add(row);
					row = new Vector<Integer>();
					rowItr = 0;
				}
			}
		}
		else {
			System.out.println("ERROR:  File has invalid format!");
			System.out.println("Generating Default Grid...");
			createDefaultGrid();
		}
	}
	
//=============================================================================	
	/**
	 * Determine if an input file is of valid format
	 * @param file
	 * @throws FileNotFoundException 
	 */
	public boolean fileIsValid(File file) throws FileNotFoundException {
		Scanner reader = new Scanner(file);
		boolean flag = true;
		
		// First, make sure all elements are integers
		while(reader.hasNext()) {
			try {
				Integer.parseInt(reader.next());
			}
			catch (NumberFormatException e) {
				flag = false;
				break;
			}
		}
		reader.close();
		
		// Next, make sure matrix rows have equal # of elements
		if(flag == true) {
			reader = new Scanner(file);
			Set<Integer> rowElems = new HashSet<Integer>();
			while(reader.hasNextLine()) {
				char[] currLine = reader.nextLine().toCharArray();
				String elem = new String();
				Vector<String> row = new Vector<String>();
				for(int i=0; i<currLine.length; i++) {
					if((currLine[i] == ' ') || (currLine[i] == '\n')) {
						if(!elem.equals("")) {
							row.add(elem);
							elem = new String();
						}
					}
					else {
						elem += currLine[i];
					}
				}
				if(!(elem.contains(" ") || elem.contains("\n") || elem.equals(""))) {
					row.add(elem);
				}
				//System.out.println(row);
				rowElems.add(row.size());
			}
			if(rowElems.size() != 1) {
				flag = false;
			}
		}
		
		return flag;
	}
	
//=============================================================================
	/**
	 * Return a Vector of unique integers found in the Grid
	 * @return uniqueInts
	 */
	public Vector<Integer> getUniqueInts() {
		Vector<Integer> uniqueInts = new Vector<Integer>();
		Set<Integer> elements = new HashSet<Integer>();
		for(int i=0; i<this.grid.size(); i++) {
			for(int j=0; j<this.grid.get(i).size(); j++) {
				elements.add(this.grid.get(i).get(j));
			}
		}
		for(Integer elem : elements) {
			uniqueInts.add(elem);
		}
		//System.out.print(uniqueInts);
		
		return uniqueInts;
	}
	
//=============================================================================	
	/**
	 * Create a string representation of the Grid
	 */
	@Override
	public String toString() {
		int maxLength = 0;
		for(int i=0; i<this.grid.size(); i++) {
			for(int j=0; j<this.grid.get(i).size(); j++) {
				Integer val = new Integer(this.grid.get(i).get(j));
				if(maxLength < val.toString().length()) {
					maxLength = val.toString().length();
				}
			}
		}
		
		String string = new String();
		for(int i=0; i<this.grid.size(); i++) {
			for(int j=0; j<this.grid.get(i).size(); j++) {
				Integer val = new Integer(this.grid.get(i).get(j));
				string += val.toString();
				int numSpaces = maxLength+1 - val.toString().length();
				if(j != this.grid.get(i).size()-1) {
					for(int k=0; k<numSpaces; k++) {
						string += " ";
					}
				}
			}
			string += "\n";
		}
		return string;
	}
	
//=============================================================================
	/**
	 * String representation of the grid location positions.
	 */
	public String gridPositions() {
		String string = new String();
		final Integer maxNumLength = new Integer(
				(this.grid.size() * this.grid.get(0).size())).toString().length();
		Vector<Vector<Integer>> positions = this.getPositionMatrix();
		
		for(int i=0; i<positions.size(); i++) {
			for(int j=0; j<positions.get(i).size(); j++) {
				string += positions.get(i).get(j);
				if(j != positions.get(i).size()-1) {
					int spaces = (maxNumLength + 1) - 
							(new Integer(positions.get(i).get(j)).toString().length());
					for(int k=0; k<spaces; k++) {
						string += " ";
					}
				}
			}
			string += "\n";
		}
		
		return string;
	}
	
//=============================================================================
	/**
	 * Convenient display for the Grid and the grid locations.
	 */
	public void display() {
		System.out.println("GRID DATA:");
		System.out.print(this.toString());
		System.out.println();
		System.out.println("GRID POSITIONS:");
		System.out.print(this.gridPositions());
		System.out.println();
	}
	
//=============================================================================
	/**
	 * Create a map of all positions (keys) to Grid integers (values).
	 * @return gridMap
	 */
	public Map<Integer, Integer> mapGrid() {
		Map<Integer, Integer> gridMap = new HashMap<Integer, Integer>();
		int pos = 0;  // Top left position (initial)
		for(int i=0; i<grid.size(); i++) {
			for(int j=0; j<grid.get(i).size(); j++) {
				gridMap.put(pos, grid.get(i).get(j));
				pos += 1;
			}
		}
			
		return gridMap;
	}
	
//=============================================================================
	/**
	 * Given an integer, return a vector of all positions in the Grid.
	 * If the integer does not exist in the grid, return an empty Vector.
	 * @param integer
	 * @return intList
	 */
	public Vector<Integer> intPositions(Integer integer) {
		Vector<Integer> intList = new Vector<Integer>();
		Map<Integer, Integer> map = this.mapGrid();
		Set<Integer> positions = map.keySet();
		for(Integer pos : positions) {
			if(map.get(pos).equals(integer)) {
				intList.add(pos);
			}
		}
		
		return intList;
	}
	
//=============================================================================
	/**
	 * Given an integer, return a Vector all of the horizontal connections 
	 * in the Grid. Each connection is a Vector of position integers.
	 * @param integer
	 * @return connections
	 */
	public Vector<Vector<Integer>> horizontalConnections(Integer integer) {
		Vector<Vector<Integer>> connections = new Vector<Vector<Integer>>();
		Vector<Integer> availPositions = this.intPositions(integer);
		int pos = 0;
		
		for(int i=0; i<this.grid.size(); i++) {
			Vector<Integer> conn = new Vector<Integer>();
			for(int j=0; j<this.grid.get(i).size(); j++) {
				if(availPositions.contains(pos)) {
					conn.add(pos);
					if(j != this.grid.get(i).size()-1) {
						if(!availPositions.contains(pos+1)) {
							if(!conn.isEmpty()) {
								connections.add(conn);
								conn = new Vector<Integer>();
							}
						}
					}
					else {
						if(!conn.isEmpty()) {
							connections.add(conn);
							conn = new Vector<Integer>();
						}
					}
				}
				pos += 1;
			}
		}
		
		return connections;
	}
	
//=============================================================================
	/**
	 * Given an integer, return a Vector all of the vertical connections 
	 * in the Grid. Each connection is a Vector of position integers.
	 * @param integer
	 * @return connections
	 */
	public Vector<Vector<Integer>> verticalConnections(Integer integer) {
		Vector<Vector<Integer>> connections = new Vector<Vector<Integer>>();
		Vector<Integer> availPositions = this.intPositions(integer);
		Vector<Vector<Integer>> positions = this.transpose(this.getPositionMatrix());
		Vector<Vector<Integer>> transGrid = this.transpose(this.grid);
		
		for(int i=0; i<transGrid.size(); i++) {
			Vector<Integer> conn = new Vector<Integer>();
			for(int j=0; j<transGrid.get(i).size(); j++) {
				if(availPositions.contains(positions.get(i).get(j))) {
					conn.add(positions.get(i).get(j));
					if(j != transGrid.get(i).size()-1) {
						if(!availPositions.contains(positions.get(i).get(j+1))) {
							if(!conn.isEmpty()) {
								connections.add(conn);
								conn = new Vector<Integer>();
							}
						}
					}
					else {
						if(!conn.isEmpty()) {
							connections.add(conn);
							conn = new Vector<Integer>();
						}
					}
				}
			}
		}
		
		return connections;
	}
	
//=============================================================================
	/**
	 * Return all of the connections for a particular integer in the Grid.
	 * Connections are positions where integers of the same value neighbor each
	 * other to the left/right/top/bottom (no diagonals).
	 * @param integer
	 * @return connections
	 */
	//NEEDS IMPROVEMENT TODO
	public Vector<Vector<Integer>> findConnections(Integer integer) {
		Vector<Vector<Integer>> hConn = this.horizontalConnections(integer);
		Vector<Vector<Integer>> vConn = this.verticalConnections(integer);
		Vector<Vector<Integer>> connections = new Vector<Vector<Integer>>();

		for(int i=0; i<hConn.size(); i++) {
			//System.out.println("Current Vector: " + hConn.get(i));
			Set<Integer> connSet = new HashSet<Integer>();
			for(int j=0; j<hConn.get(i).size(); j++) {
				connSet.add(hConn.get(i).get(j));
				//System.out.println("  Added: " + hConn.get(i).get(j));
				for(int x=0; x<vConn.size(); x++) {
					if(vConn.get(x).contains(hConn.get(i).get(j))) {
						for(int y=0; y<vConn.get(x).size(); y++) {
							connSet.add(vConn.get(x).get(y));
							//System.out.println("  Added: " + vConn.get(x).get(y));
						}
					}
				}
			}
			//System.out.println("  connSet: " + connSet);
			Vector<Integer> connection = new Vector<Integer>();
			for(int elem : connSet) {
				connection.add(elem);
			}
			connections.add(connection);
		}
		
		// This will return duplicates and subsets of connections but still works
		return connections;
	}
	
//=============================================================================	
	/**
	 * Given an integer, return a Vector of Vectors of positions in the 
	 * Grid that satisfy the maximum number of connected integers 
	 * of the supplied value.
	 * @param integer
	 * @return connections
	 */
	public Vector<Vector<Integer>> maxConnections(Integer integer) {
		Vector<Vector<Integer>> connections = new Vector<Vector<Integer>>();
		Vector<Vector<Integer>> allConnections = this.findConnections(integer);
		int max = 0;
		for(int i=0; i<allConnections.size(); i++) {
			if(allConnections.get(i).size() > max) {
				max = allConnections.get(i).size();
			}
		}
		for(int i=0; i<allConnections.size(); i++) {
			if(allConnections.get(i).size() == max) {
				connections.add(allConnections.get(i));
			}
		}
		/*
		if(connections.size() > 1) {
			Vector<Vector<Integer>> temp = new Vector<Vector<Integer>>(connections);
			System.out.println(temp);
			int s = 0;
			for(int i=s; i<connections.size(); i++) {
				Set<Integer> conn = new HashSet<Integer>(connections.get(i));
				for(int j=0; j<connections.size(); j++) {
					if(i != j) {
						Set<Integer> toCompare = new HashSet<Integer>(connections.get(j));
						if(toCompare.equals(conn)) {
							temp.remove(j);
							temp.add(j, new Vector<Integer>());
						}
					}
				}
				s += 1;
			}
			connections = new Vector<Vector<Integer>>(temp);
		}
		*/
		
		return connections;
	}
	
//=============================================================================
	/**
	 * Return a mapping of the integer (key) with Vector consisting of Vectors
	 * of maximum connection position configurations (value). These inner 
	 * Vectors will be ties for maximum connection.
	 * @return map
	 */
	public Map<Integer, Vector<Vector<Integer>>> largestConnection() {
		Map<Integer, Vector<Vector<Integer>>> map = 
				new HashMap<Integer, Vector<Vector<Integer>>>();
		Vector<Integer> ints = this.getUniqueInts();
	
		int max = 0;
		for(int val : ints) {
			Vector<Vector<Integer>> mConns = this.maxConnections(val);
			if(mConns.get(0).size() > max) {
				max = mConns.get(0).size();
				map = new HashMap<Integer, Vector<Vector<Integer>>>();
				map.put(val, mConns);
			}
			else if(mConns.get(0).size() == max) {
				map.put(val, mConns);
			}
		}
	
		return map;
	}
	
//=============================================================================
	/**
	 * Given a Vector<Vector<Integer>>, where the inner Vector represents
	 * column values and the outer Vector represents each row, transpose it.
	 * Example:  1 2 3
	 * 			 4 5 6
	 *           7 8 9
	 * 
	 * Becomes:  1 4 7
	 *           2 5 8
	 *           3 6 9
	 *           
	 * @param matrix
	 * @return newMatrix
	 */
	public Vector<Vector<Integer>> transpose(Vector<Vector<Integer>> matrix) {
		Vector<Vector<Integer>> newMatrix = new Vector<Vector<Integer>>();
		for(int z=0; z<matrix.get(0).size(); z++) {
			Vector<Integer> transRow = new Vector<Integer>();
			for(int i=0; i<matrix.size(); i++) {
				for(int j=0; j<matrix.get(i).size(); j++) {
					if(j == z) {
						transRow.add(matrix.get(i).get(j));
					}
				}
			}
			newMatrix.add(transRow);
		}
		
		return newMatrix;
	}
	
//=============================================================================
	/**
	 * String representation of the maximum connected integers of one value.
	 * Display the integer, number of connections, and where connections are.
	 */
	//TODO
	public String showMaxInfo() {
		String string = new String();
		
		return string;
	}
	
//=============================================================================
	/**
	 * Return the position matrix for the Grid::grid
	 * @return positionMatrix
	 */
	public Vector<Vector<Integer>> getPositionMatrix() {
		Vector<Vector<Integer>> positionMatrix = new Vector<Vector<Integer>>();
		int pos = 0;
		
		for(int i=0; i<this.grid.size(); i++) {
			Vector<Integer> row = new Vector<Integer>();
			for(int j=0; j<this.grid.get(i).size(); j++) {
				row.add(pos);
				pos += 1;
			}
			positionMatrix.add(row);
		}
		
		return positionMatrix;
	}
	
//=============================================================================
	/**
	 * Return Vector<Vector<Integer>> --> Grid::grid
	 * @return this.grid
	 */
	public Vector<Vector<Integer>> getGrid() {
		return this.grid;
	}
	
//=============================================================================
	
}
