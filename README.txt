***
This is still a work in progress.
Currently there is no build manager being utilized. The code is being compiled using whatever Eclipse uses.
***

Files should be ASCII Text files, but this is not necessarily a requirement. The matrix should consist of only integers seperated by any number of blanks. Each row of the matrix must be the same length in regards to the number of integers. The last line of the file may either be the last row of the matrix or a newline. Do not add spaces or several new lines at the end of the file, as it will not be read in correctly!

This program can be used to find the maximum number of connected integers in any given matrix. Two integers are said to be connected if they are directly to the right, left, top, or bottom of another integer of the same value. Positions are used only once, meaning that if you have an arrangement such as:

4 4 
4 4

This will only count as four connected integers and not infinite, since there are only four available positions to be used.

As an example, given the following matrix, find the maximum number of connected integers:

2 1 1 1 2 3
2 2 2 2 3 3
1 2 2 3 4 1
4 4 2 3 4 1

Here is the position matrix:

0  1  2  3  4  5
6  7  8  9  10 11
12 13 14 15 16 17
18 19 20 21 22 23

----------------------------
Max Integer : 2
Connections : 8
----------------------------
Positions Set:
{
  Connection 1 : Position 0
  Connection 2 : Position 6
  Connection 3 : Position 7
  Connection 4 : Position 13
  Connection 5 : Position 17
  Connection 6 : Position 8
  Connection 7 : Position 9
  Connection 8 : Position 20
}
